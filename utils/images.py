import os
import base64
from uuid import uuid4
from moviepy.editor import *
from PIL import ImageFont, ImageDraw, Image
from utils.map_fonts import map_fonts

from PIL import Image, ImageDraw, ImageFont


def add_background_image(input_path, output_path):
    background_width = 500  # Width in pixels
    background_height = 500  # Height in pixels

    # Create a black background image
    image_to_add = Image.open(
        input_path)  # Replace with the path to your image

    # Check if the image size is smaller than the background
    if image_to_add.width > background_width or image_to_add.height > background_height:
        # Calculate the scaling factor to fit the image within the background
        width_ratio = background_width / image_to_add.width
        height_ratio = background_height / image_to_add.height
        min_ratio = min(width_ratio, height_ratio)

        # Resize the image to fit within the background
        new_width = int(image_to_add.width * min_ratio)
        new_height = int(image_to_add.height * min_ratio)
        image_to_add = image_to_add.resize((new_width, new_height), Image.ANTIALIAS)

    # Create a black background image
    background = Image.new("RGB", (background_width, background_height), "black")

    # Calculate the position to paste the image at the center
    x = (background_width - image_to_add.width) // 2
    y = (background_height - image_to_add.height) // 2

    # Paste the image onto the black background
    background.paste(image_to_add, (x, y))

    # Save the resulting image
    background.save(output_path)
    return output_path

def add_text_to_image(image_path, text, output_path, font, color):
    # Load the image
    image_path_with_bg = add_background_image(image_path, output_path)
    image = Image.open(image_path_with_bg)
    image = image.convert('RGB')
    font_style = map_fonts[font]

    # Define the font size and font file
    font_file = font_style
    font_size = 18

    # Create a copy of the original image
    text_image = image.copy()

    # Load the font
    font = ImageFont.truetype(font_file, font_size)

    # Calculate the maximum width and height for the text
    max_width = image.width - 40  # Adjust the margin as needed
    max_height = image.height

    # Split the text into multiple lines based on the maximum width
    lines = []
    current_line = ""
    words = text.split()
    for word in words:
        line_test = current_line + " " + word if current_line else word
        line_width, _ = font.getsize(line_test)
        if line_width <= max_width:
            current_line = line_test
        else:
            lines.append(current_line)
            current_line = word
    if current_line:
        lines.append(current_line)

    # Calculate the total height of the text
    total_height = len(lines) * font_size + 1

    # Calculate the starting y-position for the text
    text_y = max_height - total_height - 20  # Adjust the vertical position as needed

    # Create a transparent background image with the dimensions of the text
    background_color = (255, 255, 255, 128)  # Set the desired background color (white with transparency)
    background_image = Image.new('RGBA', (text_image.width, total_height+1), background_color)

    # Paste the background image onto the text image at the appropriate position
    text_image.paste(background_image, (0, text_y))

    # Draw each line of text on the image
    draw = ImageDraw.Draw(text_image)
    for line in lines:
        line_width, _ = font.getsize(line)
        text_x = (text_image.width - line_width) // 2
        draw.text((text_x, text_y), line, font=font, fill=color)
        text_y += font_size

    # Save the image with the added text
    text_image.save(output_path)



def create_video_from_images_and_audio(paths, output_path):
    clips = []


    # Create video clips from the images and corresponding audio
    for path in paths:
        image_path = path.get("image_path")
        audio_path = path.get("audio_path")
        image_clip = ImageClip(image_path)
        audio_clip = AudioFileClip(audio_path)

        # Set the audio of the image clip
        image_clip = image_clip.set_audio(audio_clip)

        # Set the duration of the image clip based on the duration of the audio clip
        image_clip = image_clip.set_duration(audio_clip.duration)

        clips.append(image_clip)


    # Concatenate the video clips
    final_clip = concatenate_videoclips(clips, method="compose")

    # Write the video to the output path
    final_clip.write_videofile(output_path, codec='libx264', audio_codec='aac', fps=24)

def video2base64(video_path):
    encode_string = base64.b64encode(open(video_path, "rb").read())
    os.remove(video_path)
    return encode_string
