from bark import SAMPLE_RATE, generate_audio, preload_models
from scipy.io.wavfile import write as write_wav
import time as T

preload_models()

text_prompt = """
    MAN: I have a silky smooth voice, and today I will tell you about the exercise regimen of the common sloth.
"""
start = T.time()

audio_array = generate_audio(text_prompt)
write_wav("audio.wav", SAMPLE_RATE, audio_array)

print(T.time() - start)