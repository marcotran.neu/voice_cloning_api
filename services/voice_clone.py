from TTS.api import TTS
import uuid
import base64
import os
from pydub import AudioSegment

def change_audio_speed(input_path, output_path, speed_factor):
    # Load the audio file
    audio = AudioSegment.from_file(input_path)

    # Change the speed using the speedup technique
    speeded_up_audio = audio.speedup(playback_speed=speed_factor)

    # Export the modified audio to a new file
    speeded_up_audio.export(output_path, format="wav")  # You can change the format if needed

def generate_audio_encode(input_text: str, voice_exam_path: str, lang:str, speed_factor:int = None):
    tts = TTS(model_name="tts_models/multilingual/multi-dataset/your_tts", progress_bar=False, gpu=False)
    output_audio_file = "/tmp/" + str(uuid.uuid1()) + ".wav"
    tts.tts_to_file(input_text, speaker_wav=voice_exam_path, language=lang,
                    file_path=output_audio_file)
    if speed_factor:
        change_audio_speed(output_audio_file, output_audio_file, speed_factor)
    encode_string = base64.b64encode(open(output_audio_file, "rb").read())
    os.remove(output_audio_file)
    return encode_string