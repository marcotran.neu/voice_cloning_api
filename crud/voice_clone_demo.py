from db.models import VoiceCloneDemo

from sqlalchemy.orm import Session
from typing import Optional
from datetime import datetime
from sqlalchemy import select


def create_voice_record(
        db: Session,
        voice_name: str,
        voice_record_path: str
) -> VoiceCloneDemo:
    db_voice_record = VoiceCloneDemo(
        voice_name=voice_name, voice_record_path=voice_record_path
    )
    db.add(db_voice_record)
    db.commit()
    db.refresh(db_voice_record)
    return db_voice_record


def get_voice_records(
        db: Session,
) -> VoiceCloneDemo:
    return db.query(VoiceCloneDemo).all()
