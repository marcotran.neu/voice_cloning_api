from fastapi import FastAPI, HTTPException
from sqlalchemy.exc import IntegrityError
from fastapi.middleware.cors import CORSMiddleware


def create_app(origins) -> FastAPI:
    app_ = FastAPI(
        docs_url='/'
    )
    app_.add_middleware(
        CORSMiddleware,
        allow_origins=origins,
        allow_credentials=True,
        allow_methods=["*"],
        allow_headers=["*"],
    )
    _include_routers(app_)
    _include_exception_handlers(app_)

    return app_


def _include_routers(app_: FastAPI):
    """Include one router which include all need router.py"""

    from routers.router import router

    app_.include_router(router)


def _include_exception_handlers(app_: FastAPI):
    """Include exception handlers"""

    from exceptions.handler import (
        http_exception_handler,
        integrity_error_handler
    )

    app_.add_exception_handler(HTTPException, http_exception_handler)
    app_.add_exception_handler(IntegrityError, integrity_error_handler)


origins = ['http://localhost',
           'https://remobay.asia',
           'http://localhost:8080',
           'https://beta.reelsights.com',
           'http://localhost:3000',
           'https://reelsights.com',
           'http://52.76.57.212',
           'http://54.169.92.238',
           'http://0.0.0.0:8000',
           'http://remobay.asia',
           "https://1809-52-76-57-212.ap.ngrok.io"
           'http://remobay.asia:8080',
           'http://0.0.0.0',
           'http://13.212.190.141',
           'http://13.212.190.141:8000',
           'http://52.76.57.212:8080',
           'https://52.76.57.212:8080',
           'https://api.reelsights.com',
           'http://localhost:3000',
            'https://reelsightsai.com',
           'https://www.reelsightsai.com'
           ]

app = create_app(origins)
