from db.connection import engine
from sqlalchemy.ext.automap import automap_base
from sqlalchemy import MetaData
metadata = MetaData()

Base = automap_base()
Base.prepare(engine, reflect=True, schema='voice_record')

VoiceCloneDemo = Base.classes.voice_record_demo
