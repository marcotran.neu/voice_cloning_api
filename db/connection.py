from sqlalchemy import create_engine
from sqlalchemy.orm import Session, sessionmaker
import os

DATABASE_URI = ""
engine = create_engine(DATABASE_URI, pool_pre_ping=True)
# session = Session(engine, expire_on_commit=False)
SessionLocal = sessionmaker(engine, expire_on_commit=False, class_=Session)
def get_db():
    db = SessionLocal()
    try:
        yield db
    finally:
        db.close()

# these two lines perform the "database reflection" to analyze tables and relationships




