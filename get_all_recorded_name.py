import glob
from crud import voice_clone_demo

for audio_path in sorted(glob.glob('/tmp/*.wav')):
    print(audio_path)
    db_voice_record = voice_clone_demo.create_voice_record(
        db,
        voice_name=audio_path[:-4],
        voice_record_path=audio_path
    )