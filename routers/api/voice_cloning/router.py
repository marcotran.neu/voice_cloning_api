from fastapi import APIRouter, Depends, HTTPException, Query, UploadFile, File, Request
from sqlalchemy.orm import Session
from services.voice_clone import generate_audio_encode
from fastapi import FastAPI, Body
from typing import Dict, Any, List
from db.connection import get_db
from crud import voice_clone_demo
import shutil
import io
import soundfile as sf
from pydub import AudioSegment
import glob


router = APIRouter(
    prefix='/voice_cloning',
    tags=['voice_cloning']
)


@router.head("/status")
def status():
    return {}


@router.get("/health/{name}")
async def say_hello(name: str, request: Request):
    client_host = request.client.host
    return {"message": f"Hello {name} {client_host}"}


@router.get("/get_voice_recorded")
async def get_voice_recorded(db: Session = Depends(get_db)):
    db_voice_recorded = voice_clone_demo.get_voice_records(db)
    return db_voice_recorded

@router.post("/upload_audio_record")
async def upload_audio_record(record_name, file: UploadFile = File(...), db: Session = Depends(get_db)):
    voice_name = record_name
    if file.filename.endswith('.wav'):
        save_path = f"/tmp/{voice_name}-{file.filename}"
        with open(save_path, "wb") as buffer:
            shutil.copyfileobj(file.file, buffer)
        db_voice_record = voice_clone_demo.create_voice_record(
            db,
            voice_name=voice_name,
            voice_record_path=save_path
        )

        return {"filename": file.filename}
    else:
        audio = AudioSegment.from_file(file.file)
        audio = audio.export(format="wav")
        save_path = f"/tmp/{voice_name}-{file.filename}.wav"
        with open(save_path, "wb") as out_file:
            shutil.copyfileobj(audio, out_file)
        db_voice_record = voice_clone_demo.create_voice_record(
            db,
            voice_name=voice_name,
            voice_record_path=save_path
        )

        return {"filename": file.filename}



@router.post("/generate_speech")
async def generate_speech(body: Dict[str, Any] = Body(...)):
    text = body['input_text']
    voice_record_path = body['voice_record_path']
    lang = body['language']
    if body.get('speed_factor'):
        audio_encode = generate_audio_encode(text, voice_record_path, lang, speed_factor=body.get('speed_factor'))
    else:
        audio_encode = generate_audio_encode(text, voice_record_path, lang)
    return {
        "speech_encode": audio_encode
    }


@router.get('/sync_voice_recorded')
async def sync_voice_recorded(db: Session = Depends(get_db)):
    for audio_path in sorted(glob.glob('/tmp/*.wav')):
        db_voice_record = voice_clone_demo.create_voice_record(
            db,
            voice_name=audio_path[:-4],
            voice_record_path=audio_path
        )

    return True













