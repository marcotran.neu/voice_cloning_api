from fastapi import APIRouter

from .voice_cloning.router import router as voice_cloning_router
from .video_slideshow.router import router as video_slideshow_router


router = APIRouter(
    prefix='/api'
)
router.include_router(voice_cloning_router)
router.include_router(video_slideshow_router)

