import time

from fastapi import APIRouter, Depends, HTTPException, Query, UploadFile, File
from sqlalchemy.orm import Session
from fastapi import FastAPI, Body
from typing import Dict, Any, List, Optional
from db.connection import get_db
from crud import voice_clone_demo
import shutil
import io
import soundfile as sf
from uuid import uuid4
from fastapi.responses import JSONResponse
from fastapi import status
from pydub import AudioSegment
from pydantic import BaseModel
from utils.images import add_text_to_image, create_video_from_images_and_audio, video2base64
import os
from utils.map_fonts import map_fonts
import requests


router = APIRouter(
    prefix='/video_slideshow',
    tags=['video_slideshow']
)

class ImageTextAudio(BaseModel):
    image_id: str
    text: str
    audio_id: Optional[str]
    font: str
    color: str

class ListImageTextAudio(BaseModel):
    items: List[ImageTextAudio]



@router.get("/health/{name}")
async def say_hello(name: str):
    return {"message": f"Hello {name}"}

@router.get("fonts")
async def get_fonts():
    return map_fonts


@router.post("/upload_image")
async def upload_image(image: UploadFile):
    if image.filename.split('.')[-1].lower() in ['jpg', 'png', 'jpeg']:
        image_id = str(uuid4())
        file_path = '/tmp/' + image_id + '.jpg'
        with open(file_path, "wb") as file:
            file.write(image.file.read())
        return {"image_id": image_id}
    else:
        return JSONResponse(
            status_code=400,
            content={
                "code": status.HTTP_400_BAD_REQUEST,
                "message": "Image was not valid format (jpg, png, jpeg)"}
        )


@router.post("/upload_audio")
async def upload_audio(audio: UploadFile):
    if audio.filename.endswith('wav'):
        audio_id = str(uuid4())
        file_path = '/tmp/' + audio_id + '.wav'
        with open(file_path, "wb") as buffer:
            shutil.copyfileobj(audio.file, buffer)
        return {"audio_id": audio_id}
    elif audio.filename.endswith('mp3'):
        audio = AudioSegment.from_file(audio.file)
        audio = audio.export(format="wav")
        audio_id = str(uuid4())
        file_path = '/tmp/' + audio_id + ".wav"
        with open(file_path, "wb") as out_file:
            shutil.copyfileobj(audio, out_file)
        return {"audio_id": audio_id}
    else:
        return JSONResponse(
            status_code=400,
            content={
                "code": status.HTTP_400_BAD_REQUEST,
                "message": "Audio was not valid format (wav, mp3)"}
        )


@router.post("/create_slideshow")
async def create_slideshow(input_params: ListImageTextAudio):
    list_paths = []
    audio_paths = list()
    image_paths = list()
    for input_param in input_params.items:
        image_id = input_param.image_id
        input_text = input_param.text
        audio_id = input_param.audio_id
        font = input_param.font
        color = input_param.color
        audio_paths.append('/tmp/' + audio_id + '.wav')
        image_path = '/tmp/' + image_id + '.jpg'
        output_path = '/tmp/' + image_id + '_output' + '.jpg'
        add_text_to_image(image_path, input_text, output_path, font, color)
        image_paths.append(output_path)
        list_paths.append({"image_path": output_path,
                           "audio_path": '/tmp/' + audio_id + '.wav'})

    video_output_path = '/tmp/' + str(uuid4()) + ".mp4"
    create_video_from_images_and_audio(list_paths, video_output_path)
    for audio_path in set(audio_paths):
        os.remove(audio_path)
    for image_path in set(image_paths):
        os.remove(image_path)
    videoencode = video2base64(video_output_path)
    return {"video_encode": videoencode}

@router.post("/preview_slide")
async def preview_slide(input_param: ImageTextAudio):
    image_id = input_param.image_id
    input_text = input_param.text
    font = input_param.font
    color = input_param.color
    image_path = '/tmp/' + image_id + '.jpg'
    output_path = '/tmp/' + image_id + '_output' + '.jpg'
    add_text_to_image(image_path, input_text, output_path, font, color)
    img_encode = video2base64(output_path)
    return {"img_encode": img_encode}


@router.post("/upload_file_s3")
async def upload_file_s3(file_upload: UploadFile):
    type_file = file_upload.filename.split('.')[-1].lower()
    tmp_file_path = '/tmp/' + file_upload.filename
    with open(tmp_file_path, "wb") as file:
        file.write(file_upload.file.read())
    time.sleep(2)

    url = f"/v1/s3?key=rs-blog-page-media-file/{file_upload.filename}"
    payload = open(tmp_file_path, 'rb')
    headers = {
        'Content-Type': f'image/{type_file}'
    }

    response = requests.request("PUT", url, headers=headers, data=payload)
    return {"bucket": "rs-blog-page-media-file",
            "key": file_upload.filename}


